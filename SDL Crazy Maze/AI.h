//======================================================
//	File:		AI.h
//	Author:		David Goodwin - Team Alpha
//	Edited:		Eliot Holland
//	Course:		SGPG490
//	Assignment: Project 1
//	Title:		CRAZY MAZE
//	Description:
//		Artificial Intelligence and Game Object for
//		enemies.
//======================================================
#pragma once
#ifndef _AI_H_
#define _AI_H_

#include "Engine.h"

//====================================================
//Class : AI
//====================================================
class AI
{
private:
	SDL_Texture *artint;	//ai texture
	int counter;			//decision timer
public:
	AI();
	~AI();
	SDL_Rect ai_box;
	void init( int x, int y );
	void handle_event( int px, int py );
	void show();
	bool AICollision( SDL_Rect boundaries, Brick wall[] );
	bool isActive;		//acitive flag
	bool hit;			//collision flag
	int dir;			//choosen direction
};//end AI
//======================================================
//Name:			AI()
//Description:	General Constructor
//Arguments:	None
//Modifies:		None
//Returns:		None
//======================================================
AI::AI()
{
	artint = loadTexture("AIblob.png");
}
//======================================================
//Name:			~AI()
//Description:	General Destructor
//Arguments:	None
//Modifies:		None
//Returns:		None
//======================================================
AI::~AI()
{
	SDL_DestroyTexture (artint);
	artint = NULL;
}
void AI::init(int x, int y)
{
	//set initial render location
	ai_box.x = x;
	ai_box.y = y;
	ai_box.w = 15;
	ai_box.h = 15;
	//init variables
	counter = 0;
	dir = 0;
}
//======================================================
//Name:			handle_event()
//Description:	AI decision making method
//Arguments:	px, py	( player position )
//Modifies:		dir		( AI movement direction )
//Returns:		None
//======================================================
void AI::handle_event(int px, int py) // (David Goodwin 11/2/14)
{
	int Rnum; // random number

	//Keep's AI from changing direction every frame
	if (counter > 100) 
	{
		//time between decisions is random
		counter = ( rand() % 151 );
		hit = true;
	}
	else
	{
		counter ++;
		hit = false;
	}
	
	//make a decision
	if(hit)
	{
		//reset flag
		hit = false;		
		
		// Make it random
		Rnum = (rand() % 5) + (rand() % 5) + (rand() % 5);
		
		// select direction
		switch (Rnum / 3)
		{
		case 0:	dir = NORTH;	break;
		case 1: dir = EAST;		break;
		case 2: dir = SOUTH;	break;
		case 3: dir = WEST;		break;
		case 4:
			{
				// Persue Player - DG
				if(px > ai_box.x)
				{
					dir = WEST;
				}
				else if(px < ai_box.x)
				{
					dir = EAST;
				}
				else if(py > ai_box.y)
				{
					dir = SOUTH;
				}
				else if(py < ai_box.y) 
				{
					dir = NORTH;
				}
				break;
			}// end case
		}//end switch
		Rnum = 0;
	}// end if
}// end handle_event()
//======================================================
//Name:			show()
//Description:	Displays objects to screen
//Arguments:	None
//Modifies:		None
//Returns:		None
//======================================================
void AI::show()
{
	apply_surface( artint, &ai_box );
}

bool AI::AICollision( SDL_Rect boundaries, Brick wall[] )
{
	//AI Boundries / collision checking - (David Goodwin 10/23/14)	
	if ( ( ai_box.x < boundaries.x ) || ( ai_box.x + ai_box.w > boundaries.w ) 
		|| ( ai_box.y < boundaries.y ) || ( ai_box.y + ai_box.h > boundaries.h ) )
	{
		return true;
	}
	//level dependent code
	if( stateID == STATE_LVL1 )
	{
		for (int i = 0; i < 164; ++i)
		{
			switch ( check_collision(ai_box, wall[i].brick_wall))
			{
			case true: return true; break;
			case false: break;
			}// end switch
		}// end for
	}// end if
	else if( stateID == STATE_LVL2 )
	{
		for (int i = 0; i < 265; ++i)
		{
			switch ( check_collision(ai_box, wall[i].brick_wall))
			{
			case true: return true; break;
			case false: break;
			}// end switch
		}// end for
	}// end if
	else if( stateID == STATE_LVL3 )
	{
		for (int i = 0; i < 274; ++i)
		{
			switch ( check_collision(ai_box, wall[i].brick_wall))
			{
			case true: return true; break;
			case false: break;
			}// end switch
		}// end for
	}// end if
	return false;
}
#endif