#pragma once
#ifndef _BOMB_H_
#define _BOMB_H_

#include <stdio.h>
#include "Square.h"

class Bomb : public Square
{
private:
	SDL_Texture *bomb;
	
public:
	Bomb();
	~Bomb();
	void init(int level, int size);
	void handle_event();
	void show();
	SDL_Rect bomb_wall;
	int xPos, yPos;
	bool isActive;
};
Bomb::Bomb()
{
	bomb = loadTexture("bomb4.png");
}
Bomb::~Bomb()
{
	SDL_DestroyTexture (bomb);

	bomb = NULL;
}
void Bomb::init(int xPos, int yPos)
{
	bomb_wall.x = xPos;
	bomb_wall.y = yPos;
	bomb_wall.w = 20;
	bomb_wall.h = 20;
}
void Bomb::handle_event()
{

}

void Bomb::show()
{
	apply_surface(bomb, &bomb_wall);
}
#endif