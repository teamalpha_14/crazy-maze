#pragma once
#ifndef _Bullet_H_
#define _Bullet_H_

#include "Square.h"

class Bullet : Square
{
private:
	SDL_Texture *blt;
	int lastKey;
public:
	Bullet();
	~Bullet();
	void init(int x, int y);
	void handle_event();
	void show();
	bool isActive;
	int keyState;

	SDL_Rect Bullet_box;			//used to evaluate position
};


Bullet::Bullet()
{
	blt = loadTexture("phasegun_bullet_small.png");
}

Bullet::~Bullet()
{
	SDL_DestroyTexture (blt);

	blt = NULL;
}

void Bullet::init(int x, int y)
{
	Bullet_box.x = x;
	Bullet_box.y = y;
	Bullet_box.w = 10;
	Bullet_box.h = 10;
}

void Bullet::handle_event()
{

}

void Bullet::show()
{
	apply_surface(blt, &Bullet_box);
}
#endif