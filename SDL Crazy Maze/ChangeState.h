#pragma once
#ifndef _CHANGESTATE_H_INCLUDED
#define _CHANGESTATE_H_INCLUDED

#include "Menu.h"
#include "Help.h"
#include "Over.h"
#include "Level_1.h"
#include "Level_2.h"
#include "Level_3.h"

//State object
State *currentState = NULL;

void close()
{
	//Destroy render objects
	SDL_DestroyTexture( lvl1_tex );
	SDL_DestroyTexture( lvl2_tex );
	SDL_DestroyTexture( lvl3_tex );

	lvl1_tex = NULL;
	lvl2_tex = NULL;
	lvl3_tex = NULL;

	TTF_CloseFont( font );

	delete currentState;

	TTF_Quit();
	IMG_Quit();
	Mix_Quit();
	SDL_Quit();

	SDL_DestroyRenderer (renderer);
	SDL_DestroyWindow( window );

	renderer = NULL;
	window = NULL;
	font = NULL;
}
//======================================================
//Name:			changeState()
//Description:	Facilitates the transition between states
//Arguments:	None
//Modifies:		currentState
//				nextState
//				stateID
//Returns:		None
//======================================================
void changeState()
{
	if (nextState != STATE_NULL)
	{
		//not exiting program
		if (nextState != STATE_EXIT)
		{
			//remove current state
			delete currentState;
		}

		//assign new state
		switch (nextState)
		{
		case STATE_MENU:
			currentState = new Menu();
			break;
		case STATE_HELP:
			currentState = new Help();
			break;
		
		case STATE_LVL1:
			currentState = new Level_1();
			break;

		case STATE_LVL2:
			currentState = new Level_2();
			break;

		case STATE_LVL3:
			currentState = new Level_3();
			break;
		
		case STATE_OVER:
			load_scores();
			currentState = new Over();		
			break;
		}

		stateID = nextState;		//set new state ID
		nextState = STATE_NULL;		//maintain current state
	}
}

#endif