//=====================================================
//	File:		Engine.h
//	Author:		Eliot Holland - Team Alpha
//	Course:		SGPG490
//	Assignment: Project 1
//	Title:		CRAZY MAZE
//	Description:
//		Global variables and functions are declared here
//======================================================
//Precompiler code
#pragma once
#ifndef _ENGINE_H_INCLUDED_
#define _ENGINE_H_INCLUDED_

#include<SDL.h>
#include<SDL_image.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<sstream>

const int SCREEN_WIDTH =	1024;
const int SCREEN_HEIGHT =	768;
const int SCREEN_BPP =		32;				//bytes per pixle

const int MAX_BULLETS =		5; // max bullets (David Goodwin - 11/18/14)

//Get key state
const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );

TTF_Font *font;
SDL_Color textColor = { 255, 255, 255 };

SDL_Window*		window;						//window that's rendered to
SDL_Renderer*	renderer;					//window renderer

SDL_Event event;							//event object				
	
enum GAME_STATE	{	STATE_NULL,				//used to maintain state
					STATE_MENU,				//Main Menu
					STATE_LVL1,				//Level 1
					STATE_LVL2,				//Level 2
					STATE_LVL3,				//Level 3
					STATE_OVER,				//Game Over
					STATE_HELP,				//Help Menu
					STATE_EXIT	};			//Quit

enum KEY{	W,			//for key states
			A,
			S,
			D	};

enum DIRECT{	NORTH, 
				EAST, 
				SOUTH, 
				WEST	};  // Direction

int stateID;							//state to maintain
int nextState;							//used to change states smoothly

struct time
{
	int seconds;
	int minutes;
};//end time

struct final_score_time
{
	int total_score;
	int lvl1_score;
	int lvl2_score;
	int lvl3_score;

	time lvl1_time;
	time lvl2_time;
	time lvl3_time;
};//end final_score_time

final_score_time final;		//final score keeper

//Render objects
SDL_Rect lvl1_rect;
SDL_Rect lvl2_rect;
SDL_Rect lvl3_rect;

SDL_Texture *lvl1_tex;
SDL_Texture *lvl2_tex;
SDL_Texture *lvl3_tex;

//======================================================
//Name:			init()
//Description:	Initializes all SDL components and 
//				extensions
//Arguments:	None
//Modifies:		None
//Returns:		True - if successful
//======================================================
bool init()							
{
	//Initialize SDL, return false if failed
    if( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO ) < 0 ) return false;
    
	//Create window
	window = SDL_CreateWindow( "Crazy Maze", 
								SDL_WINDOWPOS_UNDEFINED, 
								SDL_WINDOWPOS_UNDEFINED, 
								SCREEN_WIDTH, 
								SCREEN_HEIGHT, 
								SDL_WINDOW_SHOWN );

	//Check window
    if( window == NULL ) return false;
		
	//set renderer
	renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_SOFTWARE );

	//check renderer
    if( renderer == NULL ) return false;
        
    //Initialize PNG loading
    int imgFlags = IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) ) return false;

	//Initialize TTF (TrueText Font)
	if( (TTF_Init() == -1 ) ) return false;
	font = TTF_OpenFont("Outage.ttf", 28);

	//Initialize SDL_Mixer
	if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
	{
		return false;
	}

	return true;
}
/****************************************************************************
*	Name:	loadTexture()
*	Description:	Searches for and loads an image into the instance
*	Arguments:		std::string
*	Modifies:		None
*	Retruns:		newTexture -  The loaded image
*****************************************************************************/
SDL_Texture *loadTexture (std::string filename)
{
    SDL_Texture* newTexture = NULL;									//The final texture

    SDL_Surface* loadedSurface = IMG_Load( filename.c_str() );		//Load image at specified path
    if( loadedSurface == NULL )										//error check
    {
        printf( "Unable to load image %s! SDL_image Error: %s\n", filename.c_str(), IMG_GetError() );
    }
    else
    {
        //Create texture from surface pixels
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 117, 0 ) );
        newTexture = SDL_CreateTextureFromSurface( renderer, loadedSurface );
        if( newTexture == NULL )
        {
            printf( "Unable to create texture from %s! SDL Error: %s\n", filename.c_str(), SDL_GetError() );
        }

        //Get rid of old loaded surface
        SDL_FreeSurface( loadedSurface );
    }

    return newTexture;
}
void apply_surface(	SDL_Texture* source,
					SDL_Rect* dest)
{
	if (dest != NULL)
	{
		SDL_Rect renderQuad = {	dest->x,
								dest->y,
								dest->w,
								dest->h};
		SDL_RenderCopy(renderer, source, NULL, &renderQuad);
	}
	else
	{
		SDL_RenderCopy(renderer, source, NULL, NULL);
	}
}
void set_next_state(int newState)
{
	if (nextState != STATE_EXIT)
	{
		nextState = newState;
	}
}
//======================================================
//Name:			check_collision()
//Description:	Checks for a collision between two
//				objects.
//Arguments:	SDL_Rect a	-	1st object
//				SDL_Rect b	-	2nd object
//Modifies:		None
//Returns:		True		-	Collision occured
//				False		-	No collision occured
//======================================================
bool check_collision(SDL_Rect a, SDL_Rect b)
{
	//coordinates of each box's sides
	int leftA,		leftB;
	int rightA,		rightB;
	int topA,		topB;
	int bottomA,	bottomB;

	//rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	if (bottomA <= topB) return false;
	if (topA >= bottomB) return false;
	if (rightA <= leftB) return false;
	if (leftA >= rightB) return false;

	return true;
}//end check_collision()
void load_scores()
{
	//make sure memory is free
	if( lvl1_tex != NULL )
	{
		//free the memory
		SDL_DestroyTexture( lvl1_tex );
		lvl1_tex = NULL;
	}//end if

	if( lvl2_tex != NULL )
	{
		//free the memory
		SDL_DestroyTexture( lvl2_tex );
		lvl2_tex = NULL;
	}// end if

	if( lvl3_tex != NULL )
	{
		//free the memory
		SDL_DestroyTexture( lvl3_tex );
		lvl3_tex = NULL;
	}// end if

	std::stringstream lvl1;		//Level 1 score and time
	std::stringstream lvl2;		//Level 2
	std::stringstream lvl3;		//Level 3

	//temp surfaces
	SDL_Surface *lvl1_surf;
	SDL_Surface *lvl2_surf;
	SDL_Surface *lvl3_surf;

	//Render locations
	lvl1_rect.x = 375;
	lvl1_rect.y = 335;
	lvl1_rect.w = 250;
	lvl1_rect.h = 30;

	lvl2_rect.x = 375;
	lvl2_rect.y = 375;
	lvl2_rect.w = 250;
	lvl2_rect.h = 30;

	lvl3_rect.x = 375;
	lvl3_rect.y = 415;
	lvl3_rect.w = 250;
	lvl3_rect.h = 30;

	//load level 1 score and time
	if( final.lvl1_time.seconds < 10 )
	{
		lvl1 << "Level 1: " << final.lvl1_score << "  "
							<< final.lvl1_time.minutes << ":0"
							<< final.lvl1_time.seconds;
	}
	else
	{
		lvl1 << "Level 1: " << final.lvl1_score << "  "
							<< final.lvl1_time.minutes << ":"
							<< final.lvl1_time.seconds;
	}// end if
	
	if( lvl1 != NULL )
	{
		//create surface
		lvl1_surf = TTF_RenderText_Solid( font, lvl1.str().c_str(), textColor );

		if( lvl1_surf == NULL )
		{
			printf( "Unable to render surface! %s\n", TTF_GetError() );
		}// end if

		//change surface to texture
		lvl1_tex = SDL_CreateTextureFromSurface( renderer, lvl1_surf );

		if( lvl1_tex == NULL )
		{
			printf( "Unable to create texture! %s\n", SDL_GetError() );
		}// end if
	}//end if

	//load level 2 score and time
	if( final.lvl2_time.seconds < 10 )
	{
		lvl2 << "Level 2: " << final.lvl2_score << "  "
							<< final.lvl2_time.minutes << ":0"
							<< final.lvl2_time.seconds;
	}
	else
	{
		lvl2 << "Level 2: " << final.lvl2_score << "  "
							<< final.lvl2_time.minutes << ":"
							<< final.lvl2_time.seconds;
	}// end if

	if( lvl2 != NULL )
	{
		//create surface
		lvl2_surf = TTF_RenderText_Solid( font, lvl2.str().c_str(), textColor );

		if( lvl2_surf == NULL )
		{
			printf( "Unable to render surface! %s\n", TTF_GetError() );
		}// end if

		lvl2_tex = SDL_CreateTextureFromSurface( renderer, lvl2_surf );

		if( lvl2_tex == NULL )
		{
			printf( "Unable to create texture! %s\n", SDL_GetError() );
		}// end if
	}// end if

	//load level 3 score and time
	if( final.lvl3_time.seconds < 10 )
	{
		lvl3 << "Level 3: " << final.lvl3_score << "  "
							<< final.lvl3_time.minutes << ":0"
							<< final.lvl3_time.seconds;
	}
	else
	{
		lvl3 << "Level 3: " << final.lvl3_score << "  "
							<< final.lvl3_time.minutes << ":"
							<< final.lvl3_time.seconds;
	}// end if

	if( lvl3 != NULL )
	{
		//create surface
		lvl3_surf = TTF_RenderText_Solid( font, lvl3.str().c_str(), textColor );

		if( lvl3_surf == NULL )
		{
			printf( "Unable to render surface! %s\n", TTF_GetError() );
		}// end if

		lvl3_tex = SDL_CreateTextureFromSurface( renderer, lvl3_surf );

		if( lvl3_tex == NULL )
		{
			printf( "Unable to create texture! %s\n", SDL_GetError() );
		}// end if
	}// end if

	SDL_FreeSurface( lvl1_surf );
	SDL_FreeSurface( lvl2_surf );
	SDL_FreeSurface( lvl3_surf );

}//end load_scores()
#endif