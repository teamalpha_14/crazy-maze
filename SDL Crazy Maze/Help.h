#pragma once
#ifndef _HELP_H_INCLUDED_
#define _HELP_H_INCLUDED_

#include "State.h"
#include "Button.h"

class Help : public State
{
private:
	SDL_Texture *helpBack;
	SDL_Texture *menu;
	SDL_Texture *menuo;
	SDL_Texture *exit;
	SDL_Texture *exito;

	Mix_Music * help_music;

	Button menu_B;
	Button exit_B;
public:
	Help();
	~Help();
	void logic();
	void render();
	void handle_event();
};

Help::Help()
{
	helpBack =	loadTexture("Help.jpg");
	menu =		loadTexture("ClearButton.png");
	menuo =		loadTexture("ExitClear.png");
	exit =		loadTexture("ClearButton.png");
	exito =		loadTexture("ExitClear.png");

	help_music = Mix_LoadMUS( "Back_in_black.wav" );

	menu_B.init(914, 50, 62, 87, menu);
	exit_B.init(50, 50, 62, 87, exit);
}

Help::~Help()
{
	SDL_DestroyTexture(helpBack);
	SDL_DestroyTexture(menu);
	SDL_DestroyTexture(menuo);
	SDL_DestroyTexture(exit);
	SDL_DestroyTexture(exito);
	Mix_FreeMusic( help_music );

	help_music = NULL;
	helpBack =	NULL;
	menu =		NULL;
	menuo =		NULL;
	exit =		NULL;
	exito =		NULL;
}

void Help::logic()
{
	//play music
	if( Mix_PlayingMusic() == 0 )
	{
		Mix_PlayMusic( help_music, -1 );
	}//end if
}

void Help::render()
{
	apply_surface(helpBack, NULL);
	menu_B.show();
	exit_B.show();
}

void Help::handle_event()
{
	int x = 0, y = 0;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT) set_next_state(STATE_EXIT);									//window X'd out

		else if (event.type == SDL_KEYDOWN)
		{
			if (event.key.keysym.sym == SDLK_ESCAPE)											//escape key pressed
			{
				set_next_state(STATE_MENU);
			}
		}
		else if (event.type == SDL_MOUSEBUTTONDOWN)
		{
			if (event.button.button == SDL_BUTTON_LEFT)
			{
				x = event.button.x;
				y = event.button.y;


				if ((x > menu_B.box.x)															//Menu Button
					&& (x < menu_B.box.x + menu_B.box.w)
					&& (y > menu_B.box.y)
					&& (y < menu_B.box.y + menu_B.box.h))	menu_B.sprite = menuo;

				else if ((x > exit_B.box.x)														//Exit Button
					&& (x < exit_B.box.x + exit_B.box.w)
					&& (y > exit_B.box.y)
					&& (y < exit_B.box.y + exit_B.box.h))	exit_B.sprite = exito;
			}
		}
		else if (event.type == SDL_MOUSEBUTTONUP)
		{
			if (event.button.button == SDL_BUTTON_LEFT)
			{
				x = event.button.x;
				y = event.button.y;

				if ((x > menu_B.box.x)
					&& (x < menu_B.box.x + menu_B.box.w)
					&& (y > menu_B.box.y)
					&& (y < menu_B.box.y + menu_B.box.h))	set_next_state(STATE_MENU);

				else if ((x > exit_B.box.x)
					&& (x < exit_B.box.x + exit_B.box.w)
					&& (y > exit_B.box.y)
					&& (y < exit_B.box.y + exit_B.box.h))	set_next_state(STATE_EXIT);

				else
				{
					menu_B.sprite = menu;
					exit_B.sprite = exit;
				}
			}
		}
	}
}
#endif