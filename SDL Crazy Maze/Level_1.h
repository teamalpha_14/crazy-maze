//=====================================================
//	File:		Level_1.h
//	Author:		Eliot Holland - Team Alpha
//				David Goodwin
//				Karen Chavez
//	Course:		SGPG490
//	Assignment: Project 1
//	Title:		CRAZY MAZE
//	Description:
//		Level 1 state. Includes all game elements
//		and logic necessary for Level 1.
//======================================================
#pragma once

#ifndef _LEVEL_1_H_INCLUDED_
#define _LEVEL_1_H_INCLUDED_

#include <stdio.h>
#include "State.h"
#include "Square.h"
#include "Brick.h"
#include "Player.h"
#include "Scores.h"
#include "Bomb.h"
#include "Bullet.h"
#include "AI.h"

//====================================================
//Class : Level_1
//====================================================
class Level_1 : public State
{
private:
	SDL_Texture *score_tex;		//score texture
	SDL_Texture *bomb_tex;		//bomb count texture
	SDL_Texture *hide;			//used to hide game objects
	SDL_Texture *gameBack;
	SDL_Texture *maze_bound;	//new image for boundaries to
	SDL_Rect hide_rect;
	SDL_Rect bound_image;		//account for different screen res
	SDL_Rect boundaries;
	SDL_Rect plyr_rect;
	SDL_Rect mouse_rect;	//mouse click
	SDL_Rect score_rect;	//display score
	SDL_Rect bombs_rect;	//display num bombs
	Brick brick[164];		//Maze object
	Bomb bomb[10];			//Bombs on the map array
	Bomb disp_bomb;			//for display on HUD
	Player player;			//Player object
	Scores timer;			//Timer object
	AI enemy[3];	//array of enemy objects DG
	Bullet arrayofBullets[MAX_BULLETS]; // array of max bullets (David Goodwin - 11/18/14)

	Mix_Chunk *finish_wav;	//sound for crossing finish line
	Mix_Chunk *shoot_wav;
	Mix_Chunk *bomb_wav;
	Mix_Chunk *AI_wav;
	
	int xVel;	// player velocity
	int yVel;	// player velocity
	int bxVel;	// Bullet velocity
	int byVel;	// Bullet velocity
	int ai_xVel; // AI velocity
	int ai_yVel; // AI velocity
	int mouse_x;	//Mouse coordinates
	int mouse_y;

	void shoot();
	void load_maze();
	void load_bombs();
	void display_hud();
	bool use_bomb();
	int counter;
public:
	Level_1();
	~Level_1();
	void logic();
	void render();
	void handle_event();
};// end Level_1
//======================================================
//Name:			Level_1()
//Description:	General Constructor
//Arguments:	None
//Modifies:		None
//Returns:		None
//======================================================
Level_1::Level_1()
{
	//stats
	player.bomb_count = 0;
	final.lvl1_score = 0;
	//audio
	shoot_wav = Mix_LoadWAV( "Shoot_wav.wav" );
	bomb_wav = Mix_LoadWAV( "Bomb_wav.wav" );
	AI_wav = Mix_LoadWAV( "AI_wav.wav" );
	finish_wav = Mix_LoadWAV("Finish.wav");
	//textures
	gameBack = loadTexture("game_back.png");
	maze_bound = loadTexture( "maze_bound.png" );
	hide = loadTexture( "Hide.png" );
	score_tex = NULL;
	bomb_tex = NULL;
	//objects
	load_maze();
	load_bombs();
	disp_bomb.init( 10, 50 );
	player.init( 320, 560 );
	timer.init( 710, 180 );
	enemy[0].init( 340, 180 );
	enemy[1].init( 700, 400 );
	enemy[2].init( 500, 380 );

	hide_rect.x = 0;
	hide_rect.y = 0;
	hide_rect.w = 20;
	hide_rect.h = 20;

	boundaries.x = 320;
	boundaries.y = 180;
	boundaries.w = 720;
	boundaries.h = 580;
	
	bound_image.x = 315;
	bound_image.y = 175;
	bound_image.w = 410;
	bound_image.h = 410;

	//set HUD display locations
	score_rect.x = 10;
	score_rect.y = 10;
	score_rect.w = 150;
	score_rect.h = 20;

	bombs_rect.x = 40;
	bombs_rect.y = 50;
	bombs_rect.w = 50;
	bombs_rect.h = 20;

	xVel = 0;				// player velocity
	yVel = 0;
	bxVel = 0;				// bullet velocity
	byVel = 0;
	ai_xVel = 0;			// AI velocity
	ai_yVel = 0;

	for (int i = 0; i < MAX_BULLETS; i++) //makes all bullets false
	{
		arrayofBullets[i].isActive = false;
		arrayofBullets[i].init( NULL, NULL );  //just to initialize fully
	}// end for

	mouse_x = 0;
	mouse_y = 0;		//mouse location
}//end Level_1()
//======================================================
//Name:			~Level_1()
//Description:	General Destructor
//Arguments:	None
//Modifies:		None
//Returns:		None
//======================================================
Level_1::~Level_1()
{
	//textures
	SDL_DestroyTexture( score_tex );
	SDL_DestroyTexture( bomb_tex );
	SDL_DestroyTexture( gameBack );
	SDL_DestroyTexture( maze_bound );
	SDL_DestroyTexture( hide );
	hide = NULL;
	score_tex = NULL;
	bomb_tex = NULL;
	maze_bound = NULL;
	gameBack = NULL;
	//audio
	Mix_FreeChunk( finish_wav );
	Mix_FreeChunk( shoot_wav );
	Mix_FreeChunk( bomb_wav );
	Mix_FreeChunk( AI_wav );
	finish_wav = NULL;
	shoot_wav = NULL;
	bomb_wav = NULL;
	AI_wav = NULL;
}//end ~Level_1()
//======================================================
//Name:			logic()
//Description:	Handles movement and collisions 
//				of all objects
//Arguments:	None
//Modifies:		Player player
//				Brick brick
//				AI enemy
//				Bullet arrayofBullets
//				Bomb bomb
//Returns:		None
//======================================================
void Level_1::logic()
{
	timer.calculate();		//Start the timer

//===============================
//	PLAYER LOGIC
//===============================

	// X Movement
	player.player_box.x += xVel;			//move player along x axis
	
	if ((player.player_box.x < boundaries.x)		//If colliding
	|| (player.player_box.x	+ player.player_box.w   //with boundaries
	> boundaries.w))
	{
		player.player_box.x -= xVel;			//stop movement
	}//end if
	
	for (int i = 0; i < 164; ++i)				//for every brick wall
	{											//check for collision
		if( brick[i].isActive = true )
		{
			if (check_collision(player.player_box,
									brick[i].brick_wall))
			{
				player.player_box.x -= xVel;	//stop movement
			}
		}
	}//end for

	// Y Movement
	player.player_box.y += yVel;			//move player along y axis

	if ((player.player_box.y < boundaries.y)		//if colliding
	|| (player.player_box.y + player.player_box.h	//with boundaries
	> boundaries.h))
	{
		player.player_box.y -= yVel;		//stop movement
	}//end if
	for (int i = 0; i < 164; ++i)			//for every brick wall
	{										//check collision
		switch (check_collision(player.player_box, 
								brick[i].brick_wall))
		{
		case true: 
			{
				if( brick[i].isActive = true )
				{
					player.player_box.y -= yVel;//stop movement
				}
				break;
			}//end case
		case false:
			{
				break;						//no collision
			}//end case
		}//end switch
	}//end for

	// DG - MOD EH
	for (int i = 0; i < 3; ++i)				//for every enemy
	{										//check for collision
		switch (check_collision(player.player_box,	//with player
								enemy[i].ai_box))
		{
		case true:					//if yes
			{				
				player.reset();  //reset player
				break;
			}//end case
		case false:					//if no
			{
				break;	//exit loop
			}//end case
		}//end switch
	}//end for

//=============================
//	AI LOGIC
//=============================
	for (int i = 0; i < 3; ++i)		//for every enemy
	{
		enemy[i].ai_box.x += ai_xVel;
		if( enemy[i].AICollision( boundaries, brick ) )
		{
			enemy[i].ai_box.x -= ai_xVel;
			enemy[i].hit = true;
		}//end if

		enemy[i].ai_box.y += ai_yVel;
		if( enemy[i].AICollision( boundaries, brick ) )
		{
			enemy[i].ai_box.y -= ai_yVel;
			enemy[i].hit = true;
		}//end if
		
	}// end for
//=============================
//	BULLET LOGIC
//=============================
	for (int i = 0; i < MAX_BULLETS; i++) // for every bullet
	{
		//key was pressed, continue moving
		switch( arrayofBullets[i].keyState )
		{
		case W: arrayofBullets[i].Bullet_box.y -= 4; break;
		case A: arrayofBullets[i].Bullet_box.x -= 4; break;
		case S: arrayofBullets[i].Bullet_box.y += 4; break;
		case D: arrayofBullets[i].Bullet_box.x += 4; break;
		}

		if( arrayofBullets[i].isActive == true ) //if active
		{
			//check wall objects
			for( int x = 0; x < 164; ++x ) //for every brick wall
			{
				if( check_collision(	arrayofBullets[i].Bullet_box,
										brick[x].brick_wall ) )
				{
					arrayofBullets[i].isActive = false;
				}// end if		
			}// end for

			//check ai objects
			for( int x = 0; x < 3; ++x )
			{
				if( check_collision(	arrayofBullets[i].Bullet_box,
										enemy[x].ai_box ) )
				//check collision returns true
				{
					arrayofBullets[i].isActive = false;
					enemy[x].init( 711, 179 );
					enemy[x].isActive = false;
					final.lvl1_score += 100;			// add to score
					Mix_PlayChannel( -1, AI_wav, 0 );	//play audio
				}// end if
			}//end for

			//check boundaries
			if ((arrayofBullets[i].Bullet_box.y
				< boundaries.y)		
				|| (arrayofBullets[i].Bullet_box.y 
				+ arrayofBullets[i].Bullet_box.h	
				> boundaries.h))
			{
				arrayofBullets[i].isActive = false;
			}// end if
			if ((arrayofBullets[i].Bullet_box.x
				< boundaries.x)		
				|| (arrayofBullets[i].Bullet_box.x 
				+ arrayofBullets[i].Bullet_box.w	
				> boundaries.w))
			{
				arrayofBullets[i].isActive = false;
			}// end if
		}//end if
	}// end for

//============================================
//	BOMB PICKUP LOGIC
//=============================================

	for( int i = 0; i < 10; ++i ) //every bomb
	{
		//check player collision
		if( check_collision( player.player_box, bomb[i].bomb_wall ) )
		{
			final.lvl1_score += 10;  //add 10 to score
			bomb[i].init( 10, 50 );
			player.bomb_count += 1;
		}//end if
	}//end for
} //end logic()
//======================================================
//Name:			render()
//Description:	Displays objects to screen
//Arguments:	None
//Modifies:		None
//Returns:		None
//======================================================
void Level_1::render()
{
	apply_surface( gameBack, NULL );	//show background
	apply_surface( maze_bound, &bound_image );  //maze boundaries

	for (int i = 0; i < 3; ++i) enemy[i].show();  //show every enemy
	for (int i = 0; i < 10; ++i)
	{
		if( bomb[i].isActive == true )
		{
			bomb[i].show(); //For loop to display all bomb objects -KC
		}//end if
	}//end for
	for (int i = 0; i < MAX_BULLETS; ++i)
	{
		if(arrayofBullets[i].isActive == true) //if theres an active bullet
		{
			arrayofBullets[i].show(); // show it
		}//end if
	}// end for
	
	for (int i = 0; i < 164; ++i)
	{
		if( brick[i].isActive == true )
		{
			brick[i].show(); //show maze
		}
	}

	apply_surface( hide, &hide_rect );  //hide inactive elements beneath this

	timer.show(); //finish line
	player.show(); // show player on top of everything else
	display_hud();
	disp_bomb.show();
}//end render
//======================================================
//Name:			handle_event()
//Description:	Handles all keyboard and mouse events
//Arguments:	None
//Modifies:		Brick brick
//				Player player.bomb_count
//				final_score_time final
//Returns:		None
//======================================================
void Level_1::handle_event()
{
	xVel = 0;				//resets velocity every frame
	yVel = 0;

	ai_xVel = 0;			// AI velocity
	ai_yVel = 0;

	SDL_Delay(5);   //slows down movement in milliseconds
	
	for (int i = 0; i < 3; ++i)		//for every enemy
	{
		switch( enemy[i].dir )
		{
		case NORTH: ai_yVel -= 1; break;
		case EAST:	ai_xVel += 1; break;
		case SOUTH: ai_yVel += 1; break;
		case WEST:	ai_xVel -= 1; break;
		}// end switch
		
		//ai decision maker
		if( enemy[i].isActive != false )
		{
			enemy[i].handle_event(	player.player_box.x,
									player.player_box.y);
		}//end if
	}//end for

	//movement controls
	if( currentKeyStates[ SDL_SCANCODE_W] )	yVel -= 2;	//end if
	if( currentKeyStates[ SDL_SCANCODE_S] )	yVel += 2;	//end if		
	if( currentKeyStates[ SDL_SCANCODE_A] )	xVel -= 2;	//end if		
	if( currentKeyStates[ SDL_SCANCODE_D] ) xVel += 2;	//end if
	
	//================================
	//	MOUSE EVENT : BOMBS
	//================================

	while (SDL_PollEvent(&event)) //event loop
	{
		if( event.type == SDL_QUIT ) set_next_state(STATE_EXIT);//end if	//quit
		//mouse click
		else if( event.type == SDL_MOUSEBUTTONDOWN )
		{
			if( event.button.button == SDL_BUTTON_LEFT )
			{
				mouse_x = event.button.x;
				mouse_y = event.button.y;

				shoot();  // shoot bullet
			}// end if
		}// end if
	}//end while
	
	//press escape to exit back to menu
	if( currentKeyStates[ SDL_SCANCODE_ESCAPE] ) set_next_state( STATE_MENU );	//end if

	if( check_collision( player.player_box, timer.finish_box )) //crossed finish line
	{
		Mix_HaltMusic();
		//save score and time
		final.lvl1_time.minutes = timer.minutes;
		final.lvl1_time.seconds = timer.seconds;

		if( timer.minutes < 1 )
		{
			final.lvl1_score += 1000;
		}//end if
		else final.lvl1_score += 500;

		if (Mix_PlayChannel(-1, finish_wav, 0) == -1)	//play sound clip
		{
			set_next_state(STATE_MENU);		//check for error
		}//end if

		SDL_Delay(2500);				//allow sound clip to play
		set_next_state( STATE_LVL2 );	//go to next state
	}//end if
}//end hadle_event()
//======================================================
//Name:			load_maze()
//Description:	Initialize the maze
//Arguments:	None
//Modifies:		Brick brick
//Returns:		None
//======================================================
void Level_1::load_maze()
{
	//column 1     x ,  y			//column 2
	brick[0].init(320, 240);		brick[10].init(340, 200);
	brick[1].init(320, 260);		brick[11].init(340, 240);	
	brick[2].init(320, 280);		brick[12].init(340, 320);
	brick[3].init(320, 360);		brick[13].init(340, 360);
	brick[4].init(320, 380);		brick[14].init(340, 420);
	brick[5].init(320, 420);		brick[15].init(340, 500);
	brick[6].init(320, 440);		brick[16].init(340, 540);
	brick[7].init(320, 460);		brick[17].init(340, 560);
	brick[8].init(320, 480);		
	brick[9].init(320, 500);

	//column 3						//column 4
	brick[18].init(360, 180);		brick[26].init(380, 180);
	brick[19].init(360, 200);		brick[27].init(380, 220);
	brick[20].init(360, 280);		brick[28].init(380, 260);
	brick[21].init(360, 320);		brick[29].init(380, 280);
	brick[22].init(360, 360);		brick[30].init(380, 440);
	brick[23].init(360, 400);		brick[31].init(380, 460);
	brick[24].init(360, 460);		brick[32].init(380, 520);
	brick[25].init(360, 500);		brick[33].init(380, 560);

	//column 5						//column 6
	brick[34].init(400, 180);		brick[41].init(420, 180);
	brick[35].init(400, 260);		brick[42].init(420, 220);
	brick[36].init(400, 320);		brick[43].init(420, 240);
	brick[37].init(400, 360);		brick[44].init(420, 260);
	brick[38].init(400, 400);		brick[45].init(420, 300);
	brick[39].init(400, 460);		brick[46].init(420, 320);
	brick[40].init(400, 480);		brick[47].init(420, 340);
									brick[48].init(420, 400);
									brick[49].init(420, 420);
									brick[50].init(420, 520);
									brick[51].init(420, 540);

	//column 7						//column 8
	brick[52].init(440, 380);		brick[56].init(460, 200);
	brick[53].init(440, 400);		brick[57].init(460, 240);
	brick[54].init(440, 460);		brick[58].init(460, 280);
	brick[55].init(440, 480);		brick[59].init(460, 320);
									brick[60].init(460, 340);
									brick[61].init(460, 440);
									brick[62].init(460, 460);
									brick[63].init(460, 480);
									brick[64].init(460, 500);
									brick[65].init(460, 540);

	//column 9						//column 10	
	brick[66].init(480, 200);		brick[74].init(500, 260);
	brick[67].init(480, 220);		brick[75].init(500, 280);
	brick[68].init(480, 280);		brick[76].init(500, 300);
	brick[69].init(480, 340);		brick[77].init(500, 340);
	brick[70].init(480, 360);		brick[78].init(500, 400);
	brick[71].init(480, 400);		brick[79].init(500, 420);
	brick[72].init(480, 460);		brick[80].init(500, 500);
	brick[73].init(480, 540);		brick[81].init(500, 520);
									brick[82].init(500, 540);
									brick[83].init(500, 560);

	//column 11						//column 12
	brick[84].init(520, 180);		brick[91].init(540, 180);
	brick[85].init(520, 200);		brick[92].init(540, 200);
	brick[86].init(520, 240);		brick[93].init(540, 300);
	brick[87].init(520, 260);		brick[94].init(540, 320);
	brick[88].init(520, 360);		brick[95].init(540, 360);
	brick[89].init(520, 460);		brick[96].init(540, 380);
	brick[90].init(520, 520);		brick[97].init(540, 400);
									brick[98].init(540, 440);
									brick[99].init(540, 460);
									brick[100].init(540, 480);
									brick[101].init(540, 560);

	//column 13						//column 14
	brick[102].init(560, 180);		brick[109].init(580, 220);
	brick[103].init(560, 240);		brick[110].init(580, 280);
	brick[104].init(560, 280);		brick[111].init(580, 320);
	brick[105].init(560, 380);		brick[112].init(580, 340);
	brick[106].init(560, 520);		brick[113].init(580, 420);
	brick[107].init(560, 540);		brick[114].init(580, 440);
	brick[108].init(560, 560);		brick[115].init(580, 480);
									brick[116].init(580, 500);
									brick[117].init(580, 520);

	//column 15						//column 16
	brick[118].init(600, 200);		brick[126].init(620, 240);
	brick[119].init(600, 260);		brick[127].init(620, 300);
	brick[120].init(600, 280);		brick[128].init(620, 440);
	brick[121].init(600, 320);		brick[129].init(620, 460);
	brick[122].init(600, 340);		brick[130].init(620, 480);
	brick[123].init(600, 360);		brick[131].init(620, 500);
	brick[124].init(600, 380);		brick[132].init(620, 520);
	brick[125].init(600, 560);		brick[133].init(620, 560);

	//column 17						//column 18
	brick[134].init(640, 180);		brick[143].init(660, 180);
	brick[135].init(640, 220);		brick[144].init(660, 260);
	brick[136].init(640, 280);		brick[145].init(660, 280);
	brick[137].init(640, 340);		brick[146].init(660, 320);
	brick[138].init(640, 380);		brick[147].init(660, 340);
	brick[139].init(640, 420);		brick[148].init(660, 440);
	brick[140].init(640, 440);		brick[149].init(660, 480);
	brick[141].init(640, 520);		
	brick[142].init(640, 560);

	//column 19						//column 20
	brick[150].init(680, 180);		brick[160].init(700, 220);
	brick[151].init(680, 220);		brick[161].init(700, 300);
	brick[152].init(680, 260);		brick[162].init(700, 380);
	brick[153].init(680, 340);		brick[163].init(700, 480);
	brick[154].init(680, 380);		
	brick[155].init(680, 400);		
	brick[156].init(680, 440);
	brick[157].init(680, 480);		
	brick[158].init(680, 520);		
	brick[159].init(680, 540);
}// end load_maze()
//======================================================
//Name:			shoot()
//Description:	Creates bullets and sets them as active
//Arguments:	None
//Modifies:		Bullet arrayofBullets
//Returns:		None
//======================================================
void Level_1::shoot()
{
	//for every bullet
	for( int i = 0; i < MAX_BULLETS; ++i )
	{
		if( use_bomb() == false )
		{
			//check if active
			if (arrayofBullets[i].isActive == false )
			{
				//north corridor relative to player
				if( ( mouse_x > player.player_box.x - 5 )			//left boundary
					&& ( mouse_x < player.player_box.x + 25 )		//right boundary
					&& ( mouse_y < player.player_box.y + 5 ) )		//bottom boundary
				{
					//create the bullet
					arrayofBullets[i].init(	player.player_box.x + 5, 
											player.player_box.y + 5);
					//set bullet direction
					arrayofBullets[i].keyState = W;
					//set as active
					arrayofBullets[i].isActive = true;
					Mix_PlayChannel( -1, shoot_wav, 0 );
					break;
				}
				//south
				else if( ( mouse_x > player.player_box.x - 5 )		//left boundary
					&& ( mouse_x < player.player_box.x + 25 )	//right boundary
					&& ( mouse_y > player.player_box.y + 5 ) )  //bottom boundary
				{
					//create the bullet
					arrayofBullets[i].init(	player.player_box.x + 5, 
											player.player_box.y + 5);
					//set bullet direction
					arrayofBullets[i].keyState = S;
					//set as active
					arrayofBullets[i].isActive = true;
					Mix_PlayChannel( -1, shoot_wav, 0 );
					break;
				}
				//east
				else if( ( mouse_y > player.player_box.y - 5 )	//top
					&& ( mouse_y < player.player_box.y + 25 )	//bottom
					&& ( mouse_x > player.player_box.x + 5 ) )  //left
				{
					//create the bullet
					arrayofBullets[i].init(	player.player_box.x + 5, 
											player.player_box.y + 5);
					//set bullet direction
					arrayofBullets[i].keyState = D;
					//set as active
					arrayofBullets[i].isActive = true;
					Mix_PlayChannel( -1, shoot_wav, 0 );
					break;
				}
				//west
				else if( ( mouse_y > player.player_box.y - 5 )	//top
					&& ( mouse_y < player.player_box.y + 25 )	//bottom
					&& ( mouse_x < player.player_box.x + 5 ) )	//right
				{
					//create the bullet
					arrayofBullets[i].init(	player.player_box.x + 5, 
											player.player_box.y + 5);
					//set bullet direction
					arrayofBullets[i].keyState = A;
					//set as active
					arrayofBullets[i].isActive = true;
					Mix_PlayChannel( -1, shoot_wav, 0 );
					break;
				}// end if
			}// end if
		}// end if
	}// end for
}//end shoot()
//======================================================
//Name:			load_bombs()
//Description:	Initialize bombs on map
//Arguments:	None
//Modifies:		Bomb bomb
//Returns:		None
//======================================================
void Level_1::load_bombs()
{
	for(int i = 0; i < 10; ++i)// every wall
	{	
		//random coordinate within boundaries
		int x = (rand() % 20 + 16) * 20;
		int y = (rand() % 20 + 9) * 20;

		//create bomb and set to active
		bomb[i].init(x, y);
		bomb[i].isActive = true;
	}// end for
}// end load_bombs
//======================================================
//Name:			display_hud()
//Description:	Displays score, timer and bomb count
//Arguments:	None
//Modifies:		None
//Returns:		None
//======================================================
void Level_1::display_hud()
{
	std::stringstream score; //score string
	std::stringstream bombs; //bomb string

	SDL_Surface *score_surf;
	SDL_Surface *bomb_surf;

	//deallocate memory
	if( score_tex != NULL )
	{
		SDL_DestroyTexture( score_tex );
		score_tex = NULL;
	}// end if

	if( bomb_tex != NULL )
	{
		SDL_DestroyTexture( bomb_tex );
		bomb_tex = NULL;
	}// end if

	//display score
	score << "Score: " << final.lvl1_score;

	if( score != NULL )
	{
		score_surf = TTF_RenderText_Solid( font, score.str().c_str(), textColor );

		if( score_surf == NULL )
		{
			printf( "Unable to render surface! %s\n", TTF_GetError() );
		}// end if

		score_tex = SDL_CreateTextureFromSurface( renderer, score_surf );

		if( score_tex == NULL )
		{
			printf( "Unable to create Texture! %s\n", SDL_GetError() );
		}// end if
	}// end if

	apply_surface( score_tex, &score_rect );

	//display bombs
	bombs << "X " << player.bomb_count;
	
	if( bombs != NULL )
	{
		bomb_surf = TTF_RenderText_Solid( font, bombs.str().c_str(), textColor );

		if( bomb_surf == NULL )
		{
			printf( "Unable to render surface! %s\n", TTF_GetError() );
		}// end if

		bomb_tex = SDL_CreateTextureFromSurface( renderer, bomb_surf );

		if( bomb_tex == NULL )
		{
			printf( "Unable to create Texture! %s\n", SDL_GetError() );
		}// end if
	}// end if

	apply_surface( bomb_tex, &bombs_rect );

	//Destroy render objects
	SDL_FreeSurface( bomb_surf );
	SDL_FreeSurface( score_surf );
}// end display_hud()
//======================================================
//Name:			use_bomb()
//Description:	Displays score, timer and bomb count
//Arguments:	(x , y ) mouse click location
//Modifies:		None
//Returns:		True if coordinates hit a wall
//======================================================
bool Level_1::use_bomb()
{
	for( int i = 0; i < 164; ++i )// each wall
	{
		if ((mouse_x > brick[i].brick_wall.x)	//mouse click on brick													//Play Button
			&& (mouse_x < brick[i].brick_wall.x + brick[i].brick_wall.w)
			&& (mouse_y > brick[i].brick_wall.y)
			&& (mouse_y < brick[i].brick_wall.y + brick[i].brick_wall.h))
		{
			if( player.bomb_count > 0 )		//if player has bombs
			{
				--player.bomb_count;		//decrease bomb count
				final.lvl1_score += 10;		//add to score
				brick[i].init( 0, 0 );		//move brick to hidden spot
				brick[i].isActive = false;	//make inactive
				Mix_PlayChannel( -1, bomb_wav, 0 );
			}// end if
			return true;
		}// end if
	}// end for
	return false;
}// end use_bomb()
#endif