//=====================================================
//	File:		State.h
//	Author:		Eliot Holland - Team Alpha
//	Course:		SGPG490
//	Assignment: Project 1
//	Title:		CRAZY MAZE
//	Description:
//		Parent class for templates of each class with
//		required functions.
//======================================================
#pragma once

#ifndef _SQUARE_H_
#define _SQUARE_H_

#include "Engine.h"
//===================================
//Parent Class : State
//===================================
class Square
{
public:
	virtual void init(int, int) = 0;
	virtual void handle_event() = 0;
	virtual void show() = 0;
	virtual ~Square(){};
};// end Square
#endif