//=====================================================
//	File:		State.h
//	Author:		Eliot Holland - Team Alpha
//	Course:		SGPG490
//	Assignment: Project 1
//	Title:		CRAZY MAZE
//	Description:
//		Parent class for templates of each class with
//		required functions.
//======================================================
#pragma once

#ifndef _STATE_H_INCLUDED_
#define _STATE_H_INCLUDED_

#include "Engine.h"
#include "ChangeState.h"
//===================================
//Parent Class : State
//===================================
class State
{
public:
	virtual void logic() = 0;
	virtual void render() = 0;
	virtual void handle_event()= 0;
	virtual ~State(){};
};// end State
#endif